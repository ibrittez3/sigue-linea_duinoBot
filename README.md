# Proyecto Sigue Línea AVR DuinoBot.

El presente proyecto implementa un robot sigue linea mediante el uso de los dos sensores IR CNY70
del duinoBot.

## Estructura del Proyecto

La estructura del proyecto es la siguiente:

```
.
├── project.yml
├── src
│  ├── duino
│  │  ├── duinoEyes.c
│  │  ├── duinoEyes.h
│  │  ├── duinoStateMachine.c
│  │  └── duinoStateMachine.h
│  ├── lib
│  │  ├── HotWheels
│  │  │  ├── HotWheels.c
│  │  │  └── HotWheels.h
│  │  ├── IRDriver.c
│  │  └── IRDriver.h
│  ├── lineFollower.c
│  └── lineFollower.h
└── test
   ├── duino
   │  ├── test_duinoEyes.c
   │  └── test_duinoStateMachine.c
   ├── lib
   │  └── test_IRDriver.c
   └── support
      ├── stub_io.h
      ├── stub_iom1284p.h
      └── stub_sfr_defs.h
```

## Descripción de Archivos y Directorios

- `project.yml`: Archivo de configuración del proyecto.

- `src/`: Directorio de código fuente.
  - `duino/`: Código relacionado con el control de sensores y la máquina de estados.
    - `duinoEyes.c`: Implementación del manejo de sensores.
    - `duinoEyes.h`: Declaraciones para el manejo de sensores.
    - `duinoStateMachine.c`: Implementación de la máquina de estados.
    - `duinoStateMachine.h`: Declaraciones para la máquina de estados.
  - `lib/`: Librerías adicionales utilizadas en el proyecto.
    - `HotWheels/`: Libreria de terceros de control del motor.
      - `HotWheels.c`: Implementación del control de motores.
      - `HotWheels.h`: Declaraciones para el control de motores.
    - `IRDriver.c`: Implementación del controlador de sensores IR.
    - `IRDriver.h`: Declaraciones para el controlador de sensores IR.
  - `lineFollower.c`: Implementación principal del sigue línea.
  - `lineFollower.h`: Declaraciones para el sigue línea.

- `test/`: Directorio de pruebas.

## Cómo Construir

Si se dispone de [ceedling](https://www.throwtheswitch.org/ceedling)  se puede compilar el proyecto con:

```sh
ceedling release
```

Este comando compilará el código fuente y generará el archivo `lineFollower.hex` en el directorio
`build/release`.

Caso contrario, la receta de compilación se encuentra explicitada en el archivo de configuración del
proyecto.

## Contacto

ignacio_brittez@outlook.com

## Agradecimientos:

Libreria del motor: balaguer.juan@icloud.com
