/* **************************************************************************
 *
 *        Título: Seguidor de linea duino bot.
 *           Año: 2024
 *
 * Desarrollador: Ignacio Brittez
 *
 *      e-mail: ignacio_brittez@outlook.com
 *
 * ************************************************************************** */

/** \file lineFollower.c */

/* =======================================================================
 * [INCLUDES]
 * =======================================================================
 */

#include "lineFollower.h"
#include "duino/duinoEyes.h"
#include "duino/duinoStateMachine.h"
#include "lib/HotWheels/HotWheels.h"

/* =======================================================================
 * [MAIN]
 * =======================================================================
 */

int
main ()
{
    initDuinoEyes (); /* Inicializo los ojos */
    onYourMarks ();   /* Inicializo el motor */

    event_t event;
    sm_t sm;
    sm.currentState = STATE_IDLE;

    while (1)
        {
            event = duinoSMCheckEvent ();
            smHandleEvent (&sm, event);

            switch (sm.currentState)
                {
                case STATE_IDLE:
                    softStop ();
                    break;
                case STATE_ON_LINE:
                    straightForward (100);
                    break;
                case STATE_TURN_LEFT:
                    axisLTurn (100);
                    break;
                case STATE_TURN_RIGHT:
                    axisRTurn (100);
                    break;
                case STATE_REVERSE:
                    straightBack (100);
                    break;
                default:
                    hardStop ();
                    break;
                }
        }
    return 0;
}
