/* **************************************************************************
 *
 *        Título: Inicializador de ojos duinoBot
 *           Año: 2024
 *
 * Desarrollador: Ignacio Brittez
 *
 *      e-mail: ignacio_brittez@outlook.com
 *
 * ************************************************************************** */

/** \file test_duinoEyes.c */

#include "unity.h"

/* =======================================================================
 * [MOCKED HEADERS]
 * ======================================================================= */

#include "mock_IRDriver.h"

/* =======================================================================
 *  [MODULE UNDER TEST]
 * ======================================================================= */

#include "duinoEyes.h"

/* =======================================================================
 * [AUXILIAR HEADERS]
 * ======================================================================= */

/* =======================================================================
 * [MACROS]
 * =======================================================================
 */

#define TRUE 1
#define FALSE 0

/* =======================================================================
 * [TEST GLOBALS]
 * ======================================================================= */

uint8_t virtualPINA;
uint8_t virtualDDRA;
uint8_t virtualPORTA;

/* =======================================================================
 * [AUXILIAR TEST CODE]
 * ======================================================================= */

/* =======================================================================
 * [STUBS]
 * ======================================================================= */

void
stub_IRDriver_Init (volatile uint8_t *ddr, volatile uint8_t *port, uint8_t pin)
{
    *ddr &= ~MASK (pin); /* Configura el pin como entrada */
    *port |= MASK (pin); /* Inicializo el puerto en alto para evitar estado de alta impedancia  */
}

/* =======================================================================
 * [SET UP AND TEAR DOWN CODE]
 * ======================================================================= */

void
setUp (void)
{
    virtualDDRA = 0xff;
    virtualPORTA = 0x00;
    IRDriver_Init_fake.custom_fake = stub_IRDriver_Init;
}

void
tearDown (void)
{
}

/* =======================================================================
 * [TEST CODE]
 * ======================================================================= */

/* Test Description:
 * ----------------
 * Verifico que al llamar a la función DDRA se establezca en modo lectura en ambos pines de interés
 * (PINA2 y PINA3).
 */

void
test_eyesInit_0 (void)
{
    initDuinoEyes ();
    TEST_ASSERT_EQUAL (0xff & ~MASK (PINA2) & ~MASK (PINA3), DDRA);
};

/* Test Description:
 * ----------------
 * Verifico que al llamar a la función los PORTA adecuados se establezcan en alto para evitar el
 * estado de alta impedancia (PINA2 y PINA3).
 */

void
test_eyesInit_1 (void)
{
    initDuinoEyes ();
    TEST_ASSERT_EQUAL (MASK (PINA2) | MASK (PINA3), PORTA);
};

/* Test Description:
 * ----------------
 * Verifico que al llamar a la función con detección en ambos ojos, el estado se lea adecuadamente.
 */

void
test_checkDuinoEyes_0 (void)
{
    IRDriver_IsHigh_fake.return_val = TRUE;
    eyeState_t ojos;
    ojos = checkDuinoEyes ();
    TEST_ASSERT_EQUAL_UINT8 (TRUE, ojos.isLeftEyeDetecting);
    TEST_ASSERT_EQUAL_UINT8 (TRUE, ojos.isRightEyeDetecting);
};

/* Test Description:
 * ----------------
 * Verifico que al llamar a la función sin detección en ambos ojos, el estado se lea adecuadamente.
 */

void
test_checkDuinoEyes_1 (void)
{
    IRDriver_IsHigh_fake.return_val = FALSE;
    eyeState_t ojos;
    ojos = checkDuinoEyes ();
    TEST_ASSERT_EQUAL_UINT8 (FALSE, ojos.isLeftEyeDetecting);
    TEST_ASSERT_EQUAL_UINT8 (FALSE, ojos.isRightEyeDetecting);
};
