/* **************************************************************************
 *
 *        Título: Maquina de estados sigue linea duinoBot.
 *           Año: 2024
 *
 * Desarrollador: Ignacio Brittez
 *
 *      e-mail: ignacio_brittez@outlook.com
 *
 * ************************************************************************** */

/** \file test_duinoStateMachine.c */

#ifdef TEST

/* =======================================================================
 * [INCLUDES]
 * =======================================================================
 */

#include "unity.h"

/* =======================================================================
 * [MOCKED HEADERS]
 * ======================================================================= */

#include "mock_duinoEyes.h"

/* =======================================================================
 *  [MODULE UNDER TEST]
 * ======================================================================= */

#include "duinoStateMachine.h"

/* =======================================================================
 * [AUXILIAR HEADERS]
 * ======================================================================= */

/* =======================================================================
 * [MACROS]
 * =======================================================================
 */

/* =======================================================================
 * [TEST GLOBALS]
 * ======================================================================= */

/* =======================================================================
 * [STUBS]
 * ======================================================================= */

/* =======================================================================
 * [SET UP AND TEAR DOWN CODE]
 * ======================================================================= */

void
setUp (void)
{
}

void
tearDown (void)
{
}

/* =======================================================================
 * [TEST CODE]
 * ======================================================================= */

/* Test Description:
 * ----------------
 * Verifico que si ambos sensores no detectan, el duino se quede quieto.
 */

void
test_duinoSMCheckEvent_0 (void)
{
    eyeState_t state = { FALSE, FALSE };
    checkDuinoEyes_fake.return_val = state;
    TEST_ASSERT_EQUAL (EVENT_BOTH_DOWN, duinoSMCheckEvent ());
};

/* Test Description:
 * ----------------
 * Verifico que si solo el sensor izquierdo no detecta se retorne el evento adecuado.
 */

void
test_duinoSMCheckEvent_1 (void)
{
    eyeState_t state = { FALSE, TRUE };
    checkDuinoEyes_fake.return_val = state;
    TEST_ASSERT_EQUAL (EVENT_LEFT_DOWN, duinoSMCheckEvent ());
};

/* Test Description:
 * ----------------
 * Verifico que si solo el sensor derecho no detecta se retorne el evento adecuado.
 */

void
test_duinoSMCheckEvent_2 (void)
{
    eyeState_t state = { TRUE, FALSE };
    checkDuinoEyes_fake.return_val = state;
    TEST_ASSERT_EQUAL (EVENT_RIGHT_DOWN, duinoSMCheckEvent ());
};

/* Test Description:
 * ----------------
 * Verifico que si ambos sensores detectan se retorne el evento adecuado.
 */

void
test_duinoSMCheckEvent_3 (void)
{
    eyeState_t state = { TRUE, TRUE };
    checkDuinoEyes_fake.return_val = state;
    TEST_ASSERT_EQUAL (EVENT_BOTH_UP, duinoSMCheckEvent ());
};

/* Test Description:
 * ----------------
 * Verifico que si estoy en estado IDLE y llega el evento BOTH_DOWN la maquina permanezca en estado
 * STATE_IDLE.
 */

void
test_smHandleIdleState_0 (void)
{
    sm_t sm = { .currentState = STATE_IDLE };
    smHandleIdleState (&sm, EVENT_BOTH_DOWN);

    TEST_ASSERT_EQUAL (STATE_IDLE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que si estoy en estado IDLE y llega el evento BOTH_UP la maquina permanezca en estado
 * STATE_ONLINE.
 */

void
test_smHandleIdleState_1 (void)
{
    sm_t sm = { .currentState = STATE_IDLE };
    smHandleIdleState (&sm, EVENT_BOTH_UP);

    TEST_ASSERT_EQUAL (STATE_ON_LINE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que si estoy en estado IDLE y llega el evento LEFT_DOWN la maquina permanezca en estado
 * STATE_TURN_LEFT.
 */

void
test_smHandleIdleState_2 (void)
{
    sm_t sm = { .currentState = STATE_IDLE };
    smHandleIdleState (&sm, EVENT_LEFT_DOWN);

    TEST_ASSERT_EQUAL (STATE_TURN_LEFT, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que si estoy en estado IDLE y llega el evento RIGHT_DOWN la maquina permanezca en estado
 * STATE_TURN_RIGHT.
 */

void
test_smHandleIdleState_3 (void)
{
    sm_t sm = { .currentState = STATE_IDLE };
    smHandleIdleState (&sm, EVENT_RIGHT_DOWN);

    TEST_ASSERT_EQUAL (STATE_TURN_RIGHT, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado ONLINE, cuando llegue el evento BOTH_DOWN, se cambie al estado
 * REVERSE.
 */

void
test_smHandleOnlineState_0 (void)
{
    sm_t sm = { .currentState = STATE_ON_LINE };
    smHandleOnlineState (&sm, EVENT_BOTH_DOWN);
    TEST_ASSERT_EQUAL (STATE_REVERSE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado ONLINE, cuando llegue el evento LEFT_DOWN, se cambie al estado
 * TURN_LEFT.
 */

void
test_smHandleOnlineState_1 (void)
{
    sm_t sm = { .currentState = STATE_ON_LINE };
    smHandleOnlineState (&sm, EVENT_LEFT_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_LEFT, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado ONLINE, cuando llegue el evento RIGHT_DOWN, se cambie al estado
 * TURN_RIGHT.
 */

void
test_smHandleOnlineState_2 (void)
{
    sm_t sm = { .currentState = STATE_ON_LINE };
    smHandleOnlineState (&sm, EVENT_RIGHT_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_RIGHT, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado ONLINE, cuando llegue el evento BOTH_UP, el estado no se
 * modifique.
 */

void
test_smHandleOnlineState_3 (void)
{
    sm_t sm = { .currentState = STATE_ON_LINE };
    smHandleOnlineState (&sm, EVENT_BOTH_UP);
    TEST_ASSERT_EQUAL (STATE_ON_LINE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado TURN_LEFT, se cambie al estado ONLINE cuando ambos sensores
 * vean linea.
 */

void
test_smHandleTurnLeftState_0 (void)
{
    sm_t sm = { .currentState = STATE_TURN_LEFT };
    smHandleTurnLeftState (&sm, EVENT_BOTH_UP);
    TEST_ASSERT_EQUAL (STATE_ON_LINE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado TURN_LEFT, no se modifique el estado cuando el evento entrante
 * sea diferente de BOTH_UP.
 */

void
test_smHandleTurnLeftState_1 (void)
{
    sm_t sm = { .currentState = STATE_TURN_LEFT };

    smHandleTurnLeftState (&sm, EVENT_LEFT_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_LEFT, sm.currentState);

    smHandleTurnLeftState (&sm, EVENT_RIGHT_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_LEFT, sm.currentState);

    smHandleTurnLeftState (&sm, EVENT_BOTH_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_LEFT, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado TURN_RIGHT, se cambie al estado ONLINE cuando ambos sensores
 * vean linea.
 */

void
test_smHandleTurnRightState_0 (void)
{
    sm_t sm = { .currentState = STATE_TURN_RIGHT };
    smHandleTurnRightState (&sm, EVENT_BOTH_UP);
    TEST_ASSERT_EQUAL (STATE_ON_LINE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado TURN_RIGHT, no se modifique el estado cuando el evento entrante
 * sea diferente de BOTH_UP.
 */

void
test_smHandleTurnRightState_1 (void)
{
    sm_t sm = { .currentState = STATE_TURN_RIGHT };

    smHandleTurnRightState (&sm, EVENT_LEFT_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_RIGHT, sm.currentState);

    smHandleTurnRightState (&sm, EVENT_RIGHT_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_RIGHT, sm.currentState);

    smHandleTurnRightState (&sm, EVENT_BOTH_DOWN);
    TEST_ASSERT_EQUAL (STATE_TURN_RIGHT, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado REVERSE, se cambie al estado ONLINE cuando ambos sensores
 * vean linea.
 */

void
test_smHandleReverseState_0 (void)
{
    sm_t sm = { .currentState = STATE_REVERSE };
    smHandleReverseState (&sm, EVENT_BOTH_UP);
    TEST_ASSERT_EQUAL (STATE_ON_LINE, sm.currentState);
};

/* Test Description:
 * ----------------
 * Verifico que estando en el estado REVERSE, no se modifique el estado cuando el evento entrante
 * sea diferente de BOTH_UP.
 */

void
test_smHandleReverseState_1 (void)
{
    sm_t sm = { .currentState = STATE_REVERSE };

    smHandleReverseState (&sm, EVENT_LEFT_DOWN);
    TEST_ASSERT_EQUAL (STATE_REVERSE, sm.currentState);

    smHandleReverseState (&sm, EVENT_RIGHT_DOWN);
    TEST_ASSERT_EQUAL (STATE_REVERSE, sm.currentState);

    smHandleReverseState (&sm, EVENT_BOTH_DOWN);
    TEST_ASSERT_EQUAL (STATE_REVERSE, sm.currentState);
};
#endif // TEST
