/* **************************************************************************
 *
 *        Título: Driver sensor infrarojo CNY70
 *           Año: 2024
 *
 * Desarrollador: Ignacio Brittez
 *
 *      e-mail: ignacio_brittez@outlook.com
 *
 * ************************************************************************** */

/** \file */

#ifdef TEST

/* =======================================================================
 * [INCLUDES]
 * =======================================================================
 */

#include "IRDriver.h"
#include "unity.h"
#include <stdint.h>

/* =======================================================================
 * [MACROS]
 * =======================================================================
 */
#define TRUE 1
#define FALSE 0

/* =======================================================================
 * [INTERNAL DATA DEFINITION]
 * =======================================================================
 */

static uint8_t virtualDDR;
static uint8_t virtualPORT;

/* =======================================================================
 * [TEST CODE]
 * =======================================================================
 */

void
setUp (void)
{
}

void
tearDown (void)
{
}

/*
 * Verifico que ddr quede en entrada en el pin especifico
 */
void
test_IRDriver_Init_0 (void)
{
    virtualDDR = 0xff;
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL_UINT8 (0b11110111, virtualDDR);

    virtualDDR = 0b11110000 | (1 << 3);
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL_UINT8 (0b11110000, virtualDDR);
}

/*
 * Verifico que la función inicialice el port del pin en alto (reflejo).
 */
void
test_IRDriver_Init_1 (void)
{
    virtualDDR = 0x00;
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL_UINT8 (0b00001000, virtualPORT);
}

/*
 * Verifico que la función retorne TRUE cuando halla detección en el pin
 * elegido.
 */
void
test_IRDriver_IsHigh_0 (void)
{
    virtualDDR = 0x00;
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL (TRUE, IRDriver_IsHigh (&virtualPORT, 3));
}

/*
 * Verifico que la función retorne FALSE, cuando no haya detección en el PIN
 * establecido.
 */
void
test_IRDriver_IsHigh_1 (void)
{
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    virtualPORT = 0x00; /* Modifico "externamente" el valor del port */
    TEST_ASSERT_EQUAL (FALSE, IRDriver_IsHigh (&virtualPORT, 3));
}

/*
 * Verifico que la función retorne FALSE, al consultar su estado en un pin
 * incorrecto.
 */
void
test_IRDriver_IsHigh_2 (void)
{
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL (FALSE, IRDriver_IsHigh (&virtualPORT, 2));
}

/*
 * Verifico que la función retorne TRUE con múltiples inicializaciones en
 * diferentes pins.
 */
void
test_IRDriver_IsHigh_3 (void)
{
    IRDriver_Init (&virtualDDR, &virtualPORT, 1);
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    IRDriver_Init (&virtualDDR, &virtualPORT, 7);
    TEST_ASSERT_EQUAL (TRUE, IRDriver_IsHigh (&virtualPORT, 1));
    TEST_ASSERT_EQUAL (TRUE, IRDriver_IsHigh (&virtualPORT, 3));
    TEST_ASSERT_EQUAL (TRUE, IRDriver_IsHigh (&virtualPORT, 7));
}

/*
 * Verifico que la función retorne FALSE cuando halla detección en el pin
 * elegido.
 */
void
test_IRDriver_IsLow_0 (void)
{
    virtualDDR = 0x00;
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL (FALSE, IRDriver_IsLow (&virtualPORT, 3));
}

/*
 * Verifico que la función retorne TRUE, cuando no haya detección en el PIN
 * establecido.
 */
void
test_IRDriver_IsLow_1 (void)
{
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    virtualPORT = 0x00; /* Modifico "externamente" el valor del port */
    TEST_ASSERT_EQUAL (TRUE, IRDriver_IsLow (&virtualPORT, 3));
}

/*
 * Verifico que la función retorne TRUE, al consultar su estado en un pin
 * incorrecto.
 */
void
test_IRDriver_IsLow_2 (void)
{
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    TEST_ASSERT_EQUAL (TRUE, IRDriver_IsLow (&virtualPORT, 2));
}

/*
 * Verifico que la función retorne FALSE con múltiples inicializaciones en
 * diferentes pins.
 */
void
test_IRDriver_IsLow_3 (void)
{
    IRDriver_Init (&virtualDDR, &virtualPORT, 1);
    IRDriver_Init (&virtualDDR, &virtualPORT, 3);
    IRDriver_Init (&virtualDDR, &virtualPORT, 7);
    TEST_ASSERT_EQUAL (FALSE, IRDriver_IsLow (&virtualPORT, 1));
    TEST_ASSERT_EQUAL (FALSE, IRDriver_IsLow (&virtualPORT, 3));
    TEST_ASSERT_EQUAL (FALSE, IRDriver_IsLow (&virtualPORT, 7));
}

#endif // TEST
